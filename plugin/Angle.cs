﻿using System;
using System.Windows;
using System.Windows.Forms;
using System.Collections.Generic;

namespace plugin
{
	/// <summary>
	/// Class that holds data necessary to create an angle model.
	/// </summary>
	public class Angle
	{
		/// <summary>
		/// Diameter of holes, millimeters. [1..Floor(2/3 * Min(Length, Width, Height))]
		/// </summary>
		private readonly ConstrainedValue _diameter;
		
		/// <summary>
		/// Total height, millimeters. [10..200]
		/// </summary>
		private readonly ConstrainedValue _height;
		
		/// <summary>
		/// Total length, millimeters.[10..2000]
		/// </summary>
		private readonly ConstrainedValue _length;
		
		/// <summary>
		/// Number of holes per single row. [1..Floor(2/3 * Width/Diameter)]
		/// </summary>
		private readonly ConstrainedValue _perRow;
		
		/// <summary>
		/// Number of rows on horizontal plane. [1..Floor(2/3 * Length/Diameter)]
		/// </summary>
		private readonly ConstrainedValue _rowsBottom;
		
		/// <summary>
		/// Number of rows on vertical plane. [1..Floor(2/3 * Height/Diameter)]
		/// </summary>
		private readonly ConstrainedValue _rowsTop;
		
		/// <summary>
		/// Total width, millimeters.
		/// </summary>
		private readonly ConstrainedValue _width;
		
		/// <summary>
		/// Angle thickness, millimeters. [1..Floor(Min(Length, Width, Height) / 10)]
		/// </summary>
		private readonly ConstrainedValue _thickness;
		
		/// <summary>
		/// Property to access length from outside.
		/// </summary>
		public int Length
		{
			get 
			{
				return _length.Value;
			}
			set
			{
				_length.Value = value;
				UpdateDependentOnDimesions();
			}
		}
		
		/// <summary>
		/// Property to access width from outside.
		/// </summary>
		public int Width
		{
			get 
			{
				return _width.Value;
			}
			set
			{
				_width.Value = value;
				UpdateDependentOnDimesions();
			}
		}
		
		/// <summary>
		/// Property to access height from outside.
		/// </summary>
		public int Height
		{
			get 
			{
				return _height.Value;
			}
			set
			{
				_height.Value = value;
				UpdateDependentOnDimesions();
			}
		}
		
		/// <summary>
		/// Property to access diameter from outside.
		/// </summary>
		public int Diameter
		{
			get 
			{
				return _diameter.Value;
			}
			set
			{
				_diameter.Value = value;
				int newMaxPerRow = 2 * Width / (3 * Diameter);
				int newMaxRowsTop = 2 * Height / (3 * Diameter);
				int newMaxRowsBottom = 2 * Length / (3 * Diameter);
					
				_perRow.Max = newMaxPerRow;
				_rowsTop.Max = newMaxRowsTop;
				_rowsBottom.Max = newMaxRowsBottom;
					
			}
		}
		
		/// <summary>
		/// Property to access number of holes per row from outside.
		/// </summary>
		public int PerRow
		{
			get 
			{
				return _perRow.Value;
			}
			set 
			{
				_perRow.Value = value;
			}
		}
		
		/// <summary>
		/// Property to access number of rows on vertical plane from outside.
		/// </summary>
		public int RowsTop
		{
			get 
			{
				return _rowsTop.Value;
			}
			set 
			{
				_rowsTop.Value = value;
			}
		}
		
		/// <summary>
		/// Property to access number of rows on horizontal plane from outside.
		/// </summary>
		public int RowsBottom
		{
			get 
			{
				return _rowsBottom.Value;
			}
			set 
			{
				_rowsBottom.Value = value;
			}
		}
		
		/// <summary>
		/// Property to access thickness from outside.
		/// </summary>
		public int Thickness
		{
			get 
			{
				return _thickness.Value;
			}
			set 
			{
				_thickness.Value = value;
			}
		}
		
		/// <summary>
		/// Property to store whether the angle should be rounded.
		/// </summary>
		public bool Rounded
		{
			get; set;
		}
		
		/// <summary>
		/// Generates predifined Angle 20×20×20 with two diameter 10 holes 
		/// </summary>
		public Angle()
		{
			_length = new ConstrainedValue(10, 20, 200);
			// not an error, it actually can be up to 2000
			_width = new ConstrainedValue(10, 20, 2000);
			_height = new ConstrainedValue(10, 20, 200);
			
			_thickness = new ConstrainedValue(1, 2, 2);
			
			// 13 is (2 / 3) * 20 rounded down
			_diameter = new ConstrainedValue(1, 10, 13); 
			
			// (2 / 3) * (20 / 10) rounded down is 1
			_perRow = new ConstrainedValue(1, 1, 1); 
			// also applies to these
			_rowsBottom = new ConstrainedValue(1, 1, 1);
			_rowsTop = new ConstrainedValue(1, 1, 1);
			
			Rounded = false;
		}
		
		/// <summary>
		/// Generates evenly distributed coordinates on a given line segment.
		/// For instance, GetCoords(0, 1, 3) generates (0.25, 0.5, 0.75).
		/// </summary>
		/// <param name="start">Start coordinate of the segment.</param>
		/// <param name="finish">End coordinate of the segment.</param>
		/// <param name="count">Number of coordinates to generate. Must be >= 1.
		/// ArgumentException is thrown otherwise.</param>
		/// <returns>List of generated coordinates.</returns>
		private static List<double> GetCoords(double start, double finish,
		                                      int count)
		{
			if (count < 1)
			{
				throw new ArgumentException(
					"Count of point to generate on segment must be at least 1");
			}
			
			var ret = new List<double>();
			
			double step = (finish - start) / (count + 1);
			
			for (var i = 0; i < count; i++)
			{
				ret.Add(start + (i + 1) * step);
			}
			
			return ret;
		}
		
		/// <summary>
		/// Generates coordinates for hole centers on horizontal plane.
		/// The following is assumed: first coordinate of points 
		/// is width left-to-right, second is length bottom-to-top. 
		/// (0, 0) is leftmost bottommost point of horizontal plane 
		/// (when viewed from top)
		/// </summary>
		/// <returns>List of points.</returns>
		public List<Point> GetBottomCirclePoints()
		{
			return GenerateCoords(Length, RowsBottom);
		}
		
		/// <summary>
		/// Generates coordinates for hole centers on vertical plane.
		/// The following is assumed: first coordinate of points
		/// is width left-to-right, second is height bottom-to-top. 
		/// (0, 0) is leftmost bottommost point of vertical plane
		/// (when viewed head-on)
		/// </summary>
		/// <returns>List of points.</returns>
		public List<Point> GetTopCirclePoints()
		{
			return GenerateCoords(Height, RowsTop);
		}
		
		// my architecture skill is one hundred forty six precent :(
		
		/// <summary>
		/// Actual code that generates points, moved away since 
		/// it's almost identical in both functions.
		/// </summary>
		/// <param name="nonWidth">Value of used non-width dimension,
		/// e.g length or height.</param>
		/// <param name="rows">Rows on plane coordinates are generated for.</param>
		/// <returns>List of coordinates.</returns>
		private List<Point> GenerateCoords(int nonWidth, int rows)
		{
			
			var ret = new List<Point>();
			
			// pending, width of alternating row is (num / denum) * width
			const double num = 17.0d;
			const double denum = 19.0d;
			
			// accounting for some area stolen by another plane
			int workable = nonWidth - Thickness;
			
			var lCoords = GetCoords(0, workable, rows);
			
			// if there are many rows, they are in alternating order
			if (lCoords.Count > 1)
			{
			
				for (var i = 0; i < lCoords.Count; i++)
				{
					double start;
					double finish;
					
					// even rows are left-aligned
					if (i % 2 == 0)
					{
						start = 0;
						finish = Math.Floor((double)Width * (num / denum));
					}
					// odd rows are right-aligned
					else
					{
						start = Math.Floor((double)Width * (1 - (num / denum)));
						finish = Width;
					}
					
					var wCoords = GetCoords(start, finish, PerRow);
					
					foreach (var w in wCoords)
					{
						var p = new Point(w, lCoords[i]);
						ret.Add(p);
					}
				}
			}
			// if there's one row, it is centered
			else
			{
				// TODO remove code duplication?
				double halfFreeSpace = Math.Floor((double)Width * 
				                                  ((denum - num) / denum)) / 2.0d;
				var wCoords = GetCoords(halfFreeSpace, Width - halfFreeSpace, 
				                        PerRow);
				foreach (var w in wCoords)
				{
					var p = new Point(w, lCoords[0]);
					ret.Add(p);
				}
			}
			return ret;
		}
		
		/// <summary>
		/// Updates diameter maximum to be 2/3 of minimum dimension.
		/// Called on dimensions' setters.
		/// </summary>
		private void UpdateDiameter()
		{
			int newMaxD = Math.Min(Length, Math.Min(Width, Height)) * 2 / 3;
			_diameter.Max = newMaxD;
		}
		
		/// <summary>
		/// Updates thickness maximum to be 1/10 of minimum dimension.
		/// Called on dimensions' setters.
		/// </summary>
		private void UpdateThickness()
		{
			int newMaxT = Math.Min(Length, Math.Min(Width, Height)) / 10;
			_thickness.Max = newMaxT;
		}
		
		/// <summary>
		/// Combines updates of both parameters dependent on dimensions
		/// into a single call.
		/// </summary>
		private void UpdateDependentOnDimesions()
		{
			UpdateDiameter();
			UpdateThickness();
		}
		
		/// <summary>
		/// A crutch to somehow get minimum and maximum for constraints
		/// to display them in UI. This is bad, I know. No need to remind me.
		/// "I'm so bad (baby I don't care)" is the theme song of this project.
		/// </summary>
		/// <param name="which">Parameter enum corresponding to parameter.</param>
		/// <returns>A Point where X is minimum possible value for 
		/// textbox's value, Y is maximum.</returns>
		public Point GetMinMax(ParameterEnum which)
		{
			
			ConstrainedValue reference;
			
			// this is bad :(
			switch (which)
			{
				case ParameterEnum.Length:
					reference = _length;
					break;
				case ParameterEnum.Width:
					reference = _width;
					break;
				case ParameterEnum.Height:
					reference = _height;
					break;
				case ParameterEnum.Diameter:
					reference = _diameter;
					break;
				case ParameterEnum.PerRow:
					reference = _perRow;
					break;
				case ParameterEnum.RowsBottom:
					reference = _rowsBottom;
					break;
				case ParameterEnum.RowsTop:
					reference = _rowsTop;
					break;
				case ParameterEnum.Thickness:
					reference = _thickness;
					break;
				default:
					throw new ArgumentException("Invalid ParameterEnum value.");
			}
			
			return new Point(reference.Min, reference.Max);
		}
	}
}
