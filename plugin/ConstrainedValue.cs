﻿using System;

namespace plugin
{
	
	// so yup, i cannot into planning
	// so let's take an easy way out
	// i hate my life
	
	/// <summary>
	/// Class that stores numeric value and its lower and upper boundaries.
	/// Raises ConstrainedValueException on attempts to set the value outside
	/// of defined boundaries.
	/// </summary>
	public class ConstrainedValue
	{
		
		/// <summary>
		/// Internal field to store value in.
		/// </summary>
		private int _value;
		
		/// <summary>
		/// Internal field to store maximum in.
		/// </summary>
		private int _max;
		
		/// <summary>
		/// Internal field to store minimum in.
		/// </summary>
		private int _min;
		
		/// <summary>
		/// The lowest value that can be assigned to any of the fields.
		/// </summary>
		private const int _minimumPossible = 1;
		
		/// <summary>
		/// The highest value that can be assigned to any of the fields.
		/// </summary>
		private const int _maximumPossible = 2000;
		
		/// <summary>
		/// Lower inclusive boundary of the value as property.
		/// Throws ArgumentException when assinged to impossible (e.g greater
		/// than maximum or not in defined bounds) value.
		/// </summary>
		public int Min
		{
			get 
			{
				return _min;
			}
			set
			{
				if (value < _minimumPossible)
				{
					throw new ArgumentException(String.Format(
						"Minimum possible value for the field is {0}",
						_minimumPossible));
				}
				if (value > _maximumPossible)
				{
					throw new ArgumentException(String.Format(
						"Maximum possible value for the field is {0}",
						_maximumPossible));
				}
				if (value > Max)
				{
					throw new ArgumentException(
						"Minumum value cannot be greater than maximum," + 
						" update it first if necessary");
				}
				_min = value;
				// prevents out of bounds value when minimum is raised
				if (_value < _min)
				{
					_value = _min;
				}
			}
		}
		
		/// <summary>
		/// Upper inclusive boundary of the value as property.
		/// Throws ArgumentException when assinged to impossible (e.g less than
		/// minimum or not in defined bounds) value.
		/// </summary>
		public int Max
		{
			get 
			{
				return _max;
			}
			set
			{
				if (value < _minimumPossible)
				{
					throw new ArgumentException(String.Format(
						"Minimum possible value for the field is {0}",
						_minimumPossible));
				}
				if (value > _maximumPossible)
				{
					throw new ArgumentException(String.Format(
						"Maximum possible value for the field is {0}",
						_maximumPossible));
				}
				if (value < Min)
				{
					throw new ArgumentException(
						"Maximum value cannot be less than minimum," + 
						" update it first if necessary");
				}
				_max = value;
				// prevents out of bounds value whem maximum is lowered
				if (_value > _max)
				{
					_value = _max;
				}
			}
		}
		
		/// <summary>
		/// The value to store.
		/// Throws ConstrainedValueException with an appropriate Type set
		/// when assigned to values not in Min..Max range.
		/// </summary>
		public int Value
		{
			get 
			{
				return _value;
			}
			set
			{
				if (value > Max)
				{
					throw new ConstrainedValueException(
						ValueExceptionType.ValueTooBig);
				}
				if (value < Min)
				{
					throw new ConstrainedValueException(
						ValueExceptionType.ValueTooSmall);
				}
				
				_value = value;
			}
		}
		
		/// <summary>
		/// Constructor of ConstrainedValue.
		/// </summary>
		/// <param name="min">Minimum possible value.</param>
		/// <param name="value">Currently stored value.</param>
		/// <param name="max">Maximum possible value.</param>
		public ConstrainedValue(int min, int value, int max)
		{
			Max = max;
			Min = min;
			Value = value;
		}
	}
}
