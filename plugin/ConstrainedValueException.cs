﻿using System;

namespace plugin
{	
	/// <summary>
	/// An exception that is fired when a ConstraniedValue is assigned to invalid
	/// value. Has an enum that specifies which boundary, minimum or maximum,
	/// was violated.
	/// </summary>
	public class ConstrainedValueException : ApplicationException
	{
		
		/// <summary>
		/// The only constructor.
		/// </summary>
		/// <param name="type">Type of the exception.</param>
		public ConstrainedValueException(ValueExceptionType type)
		{
			Type = type;
		}
		
		/// <summary>
		/// Getter property for exception's type.
		/// </summary>
		public ValueExceptionType Type
		{
			get; private set;
		}
		
	}
}