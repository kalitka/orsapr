﻿using System;
using Kompas6API5;
using Kompas6Constants3D;

namespace plugin
{
	/// <summary>
	/// Class that creates Angles in Kompas3D.
	/// </summary>
	public class KompasWrapper
	{
		/// <summary>
		/// Object that holds our kompas link (i guess).
		/// </summary>
		private KompasObject _kompas = null;
		
		/// <summary>
		/// Half the width of angle, used a few times throughout this class.
		/// </summary>
		private double _halfX;
		
		/// <summary>
		/// Half the length of angle, also used.
		/// </summary>
		private double _halfY;
		
		/// <summary>
		/// A constructor that does literally nothing.
		/// </summary>
		public KompasWrapper()
		{
		}
		
		/// <summary>
		/// Public method called to create an Angle.
		/// </summary>
		/// <param name="angle">Angle to create in Kompas.</param>
		public void CreateAngle(Angle angle)
		{
			if (_kompas == null)
			{
				Type t = Type.GetTypeFromProgID("KOMPAS.Application.5");
				_kompas = (KompasObject)Activator.CreateInstance(t);
			}
			
			if (_kompas != null)
			{
				// that's also crap, but who cares as long as it works
				try 
				{
					_kompas.Visible = true;
					_kompas.ActivateControllerAPI();
				}
				catch
				{
					Type t = Type.GetTypeFromProgID("KOMPAS.Application.5");
					_kompas = (KompasObject)Activator.CreateInstance(t);
					_kompas.Visible = true;
					_kompas.ActivateControllerAPI();
				}
			}
			
			var document = (ksDocument3D)_kompas.Document3D();
			document.Create();
			
			_halfX = (double)angle.Width / 2.0d;
			_halfY = (double)angle.Length / 2.0d;
			
			GenerateBottomPart(angle);
			GenerateBottomHoles(angle);
			GenerateTopPart(angle);
			GenerateTopHoles(angle);
			
			if (angle.Rounded)
			{
				Round(angle);
			}
		}
		
		/// <summary>
		/// Generates lower plane of an Angle centered around (0, 0) on XoY plane.
		/// </summary>
		/// <param name="angle">An angle to create base for.</param>
		private void GenerateBottomPart(Angle angle)
		{
			var document = (ksDocument3D)_kompas.ActiveDocument3D();
			var part = (ksPart)document.GetPart((short)Part_Type.pTop_Part);
			var basePlane = (ksEntity)part.GetDefaultEntity(
				(short)Obj3dType.o3d_planeXOY);
			if (part != null)
			{
				var sketch = (ksEntity)part.NewEntity((short)Obj3dType.o3d_sketch);
				if (sketch != null)
				{
					sketch.name = "Эскиз основания";
					var sketchDef = (ksSketchDefinition)sketch.GetDefinition();
					if (sketchDef != null)
					{
						sketchDef.SetPlane(basePlane);
						sketch.Create();
						
						var draw = (ksDocument2D)sketchDef.BeginEdit();
						// have no idea why line segments instead of a rect
						draw.ksLineSeg(-_halfX, -_halfY, _halfX, -_halfY, 1);
						draw.ksLineSeg(_halfX, -_halfY, _halfX, _halfY, 1);
						draw.ksLineSeg(_halfX, _halfY, -_halfX, _halfY, 1);
						draw.ksLineSeg(-_halfX, _halfY, -_halfX, -_halfY, 1);
						sketchDef.EndEdit();
						
						var extr = (ksEntity)part.NewEntity(
							(short)Obj3dType.o3d_bossExtrusion);
						if (extr != null)
						{
							extr.name = "Выдавливание основания";
							var extrDef = (ksBossExtrusionDefinition)extr.GetDefinition();
							if (extrDef != null)
							{
								extrDef.directionType = (short)Direction_Type.dtNormal;
								extrDef.SetSideParam(true, (short)End_Type.etBlind,
								                     angle.Thickness);
								extrDef.SetSketch(sketch);
								extr.Create();
							}
						}
					}
				}
			}
		}
		
		/// <summary>
		/// Generates (and cuts) holes on angle's base.
		/// </summary>
		/// <param name="angle">Angle to cut holes for.</param>
		private void GenerateBottomHoles(Angle angle)
		{
			var document = (ksDocument3D)_kompas.ActiveDocument3D();
			var part = (ksPart)document.GetPart((short)Part_Type.pTop_Part);
			var basePlane = (ksEntity)part.GetDefaultEntity(
				(short)Obj3dType.o3d_planeXOY);
			if (part != null)
			{
				var sketch = (ksEntity)part.NewEntity(
					(short)Obj3dType.o3d_sketch);
				if (sketch != null)
				{
					sketch.name = "Эскиз отверстий основания";
					var sketchDef = (ksSketchDefinition)sketch.GetDefinition();
					if (sketchDef != null)
					{
						sketchDef.SetPlane(basePlane);
						sketch.Create();
						
						var draw = (ksDocument2D)sketchDef.BeginEdit();
						foreach (var circle in angle.GetBottomCirclePoints())
						{
							double d = (double)angle.Diameter / 2.0d;
							draw.ksCircle(circle.X - _halfX, circle.Y - _halfY,
							              d, 1);
						}
						sketchDef.EndEdit();
						
						var extr = (ksEntity)part.NewEntity(
							(short)Obj3dType.o3d_cutExtrusion);
						if (extr != null)
						{
							extr.name = "Вырезание отверстий в основании";
							var extrDef = (ksCutExtrusionDefinition)extr.GetDefinition();
							if (extrDef != null)
							{
								extrDef.cut = true;
								// things get a little bit tricky here
								// for whatever reason, dtReverse ignores
								//  any set params, staying 10 mm embossing
								//  regardless of them;
								// dtNormal cannot be reversed;
								// so here we are using what's left, dtMiddlePlane
								// at least it works, albeit it's an overkill
								extrDef.directionType = (short)Direction_Type.dtMiddlePlane;
								extrDef.SetSideParam(true, (short)End_Type.etBlind,
								                     2*(double)angle.Thickness);
								extrDef.SetSketch(sketch);
								extr.Create();
							}
						}
					}
				}
			}
		}
		
		/// <summary>
		/// Generates and extrudes the vertical plane of an angle.
		/// </summary>
		/// <param name="angle">Angle to generate vertical plane for.</param>
		private void GenerateTopPart(Angle angle)
		{
			var document = (ksDocument3D)_kompas.ActiveDocument3D();
			var part = (ksPart)document.GetPart((short)Part_Type.pTop_Part);
			var basePlane = (ksEntity)part.GetDefaultEntity(
				(short)Obj3dType.o3d_planeXOY);
			if (part != null)
			{
				var sketch = (ksEntity)part.NewEntity((short)Obj3dType.o3d_sketch);
				if (sketch != null)
				{
					sketch.name = "Эскиз вертикальной плоскости";
					var sketchDef = (ksSketchDefinition)sketch.GetDefinition();
					if (sketchDef != null)
					{
						sketchDef.SetPlane(basePlane);
						sketch.Create();
						
						var draw = (ksDocument2D)sketchDef.BeginEdit();
						draw.ksLineSeg(-_halfX, _halfY, _halfX, _halfY, 1);
						draw.ksLineSeg(_halfX, _halfY, _halfX, 
						               _halfY - angle.Thickness, 1);
						draw.ksLineSeg(_halfX, _halfY - angle.Thickness, -_halfX,
						               _halfY - angle.Thickness, 1);
						draw.ksLineSeg(-_halfX, _halfY - angle.Thickness, -_halfX,
						               _halfY, 1);
						sketchDef.EndEdit();
						
						var extr = (ksEntity)part.NewEntity(
							(short)Obj3dType.o3d_bossExtrusion);
						if (extr != null)
						{
							extr.name = "Выдавливание вертикальной плоскости";
							var extrDef = (ksBossExtrusionDefinition)extr.GetDefinition();
							if (extrDef != null)
							{
								extrDef.directionType = (short)Direction_Type.dtNormal;
								extrDef.SetSideParam(true, (short)End_Type.etBlind,
								                     angle.Height);
								extrDef.SetSketch(sketch);
								extr.Create();
							}
						}
					}
				}
			}
		}
		
		/// <summary>
		/// Generates and cuts holes in vertical plane.
		/// </summary>
		/// <param name="angle">Angle to generate holes in.</param>
		private void GenerateTopHoles(Angle angle)
		{
			var document = (ksDocument3D)_kompas.ActiveDocument3D();
			var part = (ksPart)document.GetPart((short)Part_Type.pTop_Part);
			var basePlane = (ksEntity)part.GetDefaultEntity(
				(short)Obj3dType.o3d_planeXOZ);
			
			var offsetPlane = (ksEntity)part.NewEntity(
				(short)Obj3dType.o3d_planeOffset);
			var offsetPlaneDef = (ksPlaneOffsetDefinition)offsetPlane.GetDefinition();
			offsetPlaneDef.direction = true;
			offsetPlaneDef.offset = _halfY - angle.Thickness;
			offsetPlaneDef.SetPlane(basePlane);
			offsetPlane.Create();
			
			if (part != null)
			{
				var sketch = (ksEntity)part.NewEntity((short)Obj3dType.o3d_sketch);
				if (sketch != null)
				{
					sketch.name = "Эскиз отверстий вертикальной плоскости";
					var sketchDef = (ksSketchDefinition)sketch.GetDefinition();
					if (sketchDef != null)
					{
						sketchDef.SetPlane(offsetPlane);
						sketch.Create();
						
						var draw = (ksDocument2D)sketchDef.BeginEdit();
						foreach (var circle in angle.GetTopCirclePoints())
						{
							double d = (double)angle.Diameter / 2.0d;
							draw.ksCircle(circle.X - _halfX, 
							              -angle.Height + circle.Y, d, 1);
							// this sign is tricky -----^
						}
						sketchDef.EndEdit();
						
						var extr = (ksEntity)part.NewEntity(
							(short)Obj3dType.o3d_cutExtrusion);
						if (extr != null)
						{
							extr.name = "Вырезание отверстий вертикальной плоскости";
							var extrDef = (ksCutExtrusionDefinition)extr.GetDefinition();
							if (extrDef != null)
							{
								extrDef.SetSketch(sketch);
								extrDef.cut = true;
								// see the long comment at GenerateBottomHoles
								//  to understand why there's dtMiddlePlane
								extrDef.directionType = (short)Direction_Type.dtMiddlePlane;
								extrDef.SetSideParam(true, (short)End_Type.etBlind,
								                     2*(double)angle.Thickness);
								extr.Create();
							}
						}
					}
				}
			}
		}
		
		/// <summary>
		/// Rounds the angle.
		/// </summary>
		/// <param name="angle">Angle parameters to use.</param>
		private void Round(Angle angle)
		{
			var document = (ksDocument3D)_kompas.ActiveDocument3D();
			var part = (ksPart)document.GetPart((short)Part_Type.pTop_Part);
			
			var fillet = (ksEntity)part.NewEntity((short)Obj3dType.o3d_fillet);
			if (fillet != null)
			{
				fillet.name = "Скругление угла";
				var filletDef = (ksFilletDefinition)fillet.GetDefinition();
				if (filletDef != null)
				{
					filletDef.radius = angle.Thickness;
					filletDef.tangent = false;
					// gets all edges of a part in an array
					var edgeArray = (ksEntityCollection)part.EntityCollection(
						(short)Obj3dType.o3d_edge);
					// filters the only edge that goes through that point
					edgeArray.SelectByPoint(0, _halfY, 0);
					// the array of edges to round
					var filletArray = (ksEntityCollection)filletDef.array();
					filletArray.Clear();
					// adding the only needed edge
					filletArray.Add(edgeArray.First());
					// and making the rounding
					fillet.Create();
				}
			}
		}
		
	}
}
