﻿
namespace plugin
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button createButton;
		private System.Windows.Forms.GroupBox holesGroupBox;
		private System.Windows.Forms.Label topRowsConstraintLabel;
		private System.Windows.Forms.Label topRowsLabel;
		private System.Windows.Forms.TextBox topRowsTextBox;
		private System.Windows.Forms.Label bottomRowsConstraintLabel;
		private System.Windows.Forms.Label bottomRowsLabel;
		private System.Windows.Forms.TextBox bottomRowsTextBox;
		private System.Windows.Forms.Label perRowConstraintLabel;
		private System.Windows.Forms.Label perRowLabel;
		private System.Windows.Forms.TextBox perRowTextBox;
		private System.Windows.Forms.Label diameterConstraintLabel;
		private System.Windows.Forms.Label diameterLabel;
		private System.Windows.Forms.TextBox diameterTextBox;
		private System.Windows.Forms.GroupBox sizesGroupBox;
		private System.Windows.Forms.Label widthConstraintLabel;
		private System.Windows.Forms.Label heightConstraintLabel;
		private System.Windows.Forms.Label lengthConstraintLabel;
		private System.Windows.Forms.Label widthLabel;
		private System.Windows.Forms.Label heightLabel;
		private System.Windows.Forms.Label lengthLabel;
		private System.Windows.Forms.TextBox widthTextBox;
		private System.Windows.Forms.TextBox heightTextBox;
		private System.Windows.Forms.TextBox lengthTextBox;
		private System.Windows.Forms.ToolTip toolTipHelp;
		private System.Windows.Forms.Label thicknessConstraintLabel;
		private System.Windows.Forms.Label thicknessLabel;
		private System.Windows.Forms.TextBox thicknessTextBox;
		private System.Windows.Forms.CheckBox roundCheckBox;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.createButton = new System.Windows.Forms.Button();
			this.holesGroupBox = new System.Windows.Forms.GroupBox();
			this.topRowsConstraintLabel = new System.Windows.Forms.Label();
			this.topRowsLabel = new System.Windows.Forms.Label();
			this.topRowsTextBox = new System.Windows.Forms.TextBox();
			this.bottomRowsConstraintLabel = new System.Windows.Forms.Label();
			this.bottomRowsLabel = new System.Windows.Forms.Label();
			this.bottomRowsTextBox = new System.Windows.Forms.TextBox();
			this.perRowConstraintLabel = new System.Windows.Forms.Label();
			this.perRowLabel = new System.Windows.Forms.Label();
			this.perRowTextBox = new System.Windows.Forms.TextBox();
			this.diameterConstraintLabel = new System.Windows.Forms.Label();
			this.diameterLabel = new System.Windows.Forms.Label();
			this.diameterTextBox = new System.Windows.Forms.TextBox();
			this.sizesGroupBox = new System.Windows.Forms.GroupBox();
			this.thicknessConstraintLabel = new System.Windows.Forms.Label();
			this.thicknessLabel = new System.Windows.Forms.Label();
			this.thicknessTextBox = new System.Windows.Forms.TextBox();
			this.widthConstraintLabel = new System.Windows.Forms.Label();
			this.heightConstraintLabel = new System.Windows.Forms.Label();
			this.lengthConstraintLabel = new System.Windows.Forms.Label();
			this.widthLabel = new System.Windows.Forms.Label();
			this.heightLabel = new System.Windows.Forms.Label();
			this.lengthLabel = new System.Windows.Forms.Label();
			this.widthTextBox = new System.Windows.Forms.TextBox();
			this.heightTextBox = new System.Windows.Forms.TextBox();
			this.lengthTextBox = new System.Windows.Forms.TextBox();
			this.toolTipHelp = new System.Windows.Forms.ToolTip(this.components);
			this.roundCheckBox = new System.Windows.Forms.CheckBox();
			this.holesGroupBox.SuspendLayout();
			this.sizesGroupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// createButton
			// 
			this.createButton.Enabled = false;
			this.createButton.Location = new System.Drawing.Point(16, 312);
			this.createButton.Name = "createButton";
			this.createButton.Size = new System.Drawing.Size(216, 40);
			this.createButton.TabIndex = 6;
			this.createButton.Text = "Создать уголок";
			this.createButton.UseVisualStyleBackColor = true;
			this.createButton.Click += new System.EventHandler(this.CreateButtonClick);
			// 
			// holesGroupBox
			// 
			this.holesGroupBox.Controls.Add(this.topRowsConstraintLabel);
			this.holesGroupBox.Controls.Add(this.topRowsLabel);
			this.holesGroupBox.Controls.Add(this.topRowsTextBox);
			this.holesGroupBox.Controls.Add(this.bottomRowsConstraintLabel);
			this.holesGroupBox.Controls.Add(this.bottomRowsLabel);
			this.holesGroupBox.Controls.Add(this.bottomRowsTextBox);
			this.holesGroupBox.Controls.Add(this.perRowConstraintLabel);
			this.holesGroupBox.Controls.Add(this.perRowLabel);
			this.holesGroupBox.Controls.Add(this.perRowTextBox);
			this.holesGroupBox.Controls.Add(this.diameterConstraintLabel);
			this.holesGroupBox.Controls.Add(this.diameterLabel);
			this.holesGroupBox.Controls.Add(this.diameterTextBox);
			this.holesGroupBox.Location = new System.Drawing.Point(16, 144);
			this.holesGroupBox.Name = "holesGroupBox";
			this.holesGroupBox.Size = new System.Drawing.Size(216, 136);
			this.holesGroupBox.TabIndex = 4;
			this.holesGroupBox.TabStop = false;
			this.holesGroupBox.Text = "Отверстия";
			// 
			// topRowsConstraintLabel
			// 
			this.topRowsConstraintLabel.Location = new System.Drawing.Point(144, 104);
			this.topRowsConstraintLabel.Name = "topRowsConstraintLabel";
			this.topRowsConstraintLabel.Size = new System.Drawing.Size(64, 20);
			this.topRowsConstraintLabel.TabIndex = 18;
			this.topRowsConstraintLabel.Text = "(1..1)";
			this.topRowsConstraintLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.topRowsConstraintLabel.Visible = false;
			// 
			// topRowsLabel
			// 
			this.topRowsLabel.Location = new System.Drawing.Point(8, 104);
			this.topRowsLabel.Name = "topRowsLabel";
			this.topRowsLabel.Size = new System.Drawing.Size(80, 20);
			this.topRowsLabel.TabIndex = 17;
			this.topRowsLabel.Text = "Рядов сверху";
			this.topRowsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// topRowsTextBox
			// 
			this.topRowsTextBox.BackColor = System.Drawing.SystemColors.Window;
			this.topRowsTextBox.Enabled = false;
			this.topRowsTextBox.Location = new System.Drawing.Point(88, 104);
			this.topRowsTextBox.MaxLength = 3;
			this.topRowsTextBox.Name = "topRowsTextBox";
			this.topRowsTextBox.Size = new System.Drawing.Size(56, 20);
			this.topRowsTextBox.TabIndex = 16;
			this.topRowsTextBox.TextChanged += new System.EventHandler(this.EditTextValueChanged);
			this.topRowsTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HandleKeyPress);
			// 
			// bottomRowsConstraintLabel
			// 
			this.bottomRowsConstraintLabel.Location = new System.Drawing.Point(144, 80);
			this.bottomRowsConstraintLabel.Name = "bottomRowsConstraintLabel";
			this.bottomRowsConstraintLabel.Size = new System.Drawing.Size(64, 20);
			this.bottomRowsConstraintLabel.TabIndex = 15;
			this.bottomRowsConstraintLabel.Text = "(1..1)";
			this.bottomRowsConstraintLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.bottomRowsConstraintLabel.Visible = false;
			// 
			// bottomRowsLabel
			// 
			this.bottomRowsLabel.Location = new System.Drawing.Point(8, 80);
			this.bottomRowsLabel.Name = "bottomRowsLabel";
			this.bottomRowsLabel.Size = new System.Drawing.Size(80, 20);
			this.bottomRowsLabel.TabIndex = 14;
			this.bottomRowsLabel.Text = "Рядов снизу";
			this.bottomRowsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// bottomRowsTextBox
			// 
			this.bottomRowsTextBox.BackColor = System.Drawing.SystemColors.Window;
			this.bottomRowsTextBox.Enabled = false;
			this.bottomRowsTextBox.Location = new System.Drawing.Point(88, 80);
			this.bottomRowsTextBox.MaxLength = 3;
			this.bottomRowsTextBox.Name = "bottomRowsTextBox";
			this.bottomRowsTextBox.Size = new System.Drawing.Size(56, 20);
			this.bottomRowsTextBox.TabIndex = 13;
			this.bottomRowsTextBox.TextChanged += new System.EventHandler(this.EditTextValueChanged);
			this.bottomRowsTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HandleKeyPress);
			// 
			// perRowConstraintLabel
			// 
			this.perRowConstraintLabel.Location = new System.Drawing.Point(144, 56);
			this.perRowConstraintLabel.Name = "perRowConstraintLabel";
			this.perRowConstraintLabel.Size = new System.Drawing.Size(64, 20);
			this.perRowConstraintLabel.TabIndex = 12;
			this.perRowConstraintLabel.Text = "(1..1)";
			this.perRowConstraintLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.perRowConstraintLabel.Visible = false;
			// 
			// perRowLabel
			// 
			this.perRowLabel.Location = new System.Drawing.Point(8, 56);
			this.perRowLabel.Name = "perRowLabel";
			this.perRowLabel.Size = new System.Drawing.Size(80, 20);
			this.perRowLabel.TabIndex = 11;
			this.perRowLabel.Text = "В ряду";
			this.perRowLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// perRowTextBox
			// 
			this.perRowTextBox.BackColor = System.Drawing.SystemColors.Window;
			this.perRowTextBox.Enabled = false;
			this.perRowTextBox.Location = new System.Drawing.Point(88, 56);
			this.perRowTextBox.MaxLength = 3;
			this.perRowTextBox.Name = "perRowTextBox";
			this.perRowTextBox.Size = new System.Drawing.Size(56, 20);
			this.perRowTextBox.TabIndex = 10;
			this.perRowTextBox.TextChanged += new System.EventHandler(this.EditTextValueChanged);
			this.perRowTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HandleKeyPress);
			// 
			// diameterConstraintLabel
			// 
			this.diameterConstraintLabel.Location = new System.Drawing.Point(144, 16);
			this.diameterConstraintLabel.Name = "diameterConstraintLabel";
			this.diameterConstraintLabel.Size = new System.Drawing.Size(64, 20);
			this.diameterConstraintLabel.TabIndex = 9;
			this.diameterConstraintLabel.Text = "(1..13)";
			this.diameterConstraintLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.diameterConstraintLabel.Visible = false;
			// 
			// diameterLabel
			// 
			this.diameterLabel.Location = new System.Drawing.Point(8, 16);
			this.diameterLabel.Name = "diameterLabel";
			this.diameterLabel.Size = new System.Drawing.Size(80, 20);
			this.diameterLabel.TabIndex = 8;
			this.diameterLabel.Text = "Диаметр, мм";
			this.diameterLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// diameterTextBox
			// 
			this.diameterTextBox.BackColor = System.Drawing.SystemColors.Window;
			this.diameterTextBox.Enabled = false;
			this.diameterTextBox.Location = new System.Drawing.Point(88, 16);
			this.diameterTextBox.MaxLength = 3;
			this.diameterTextBox.Name = "diameterTextBox";
			this.diameterTextBox.Size = new System.Drawing.Size(56, 20);
			this.diameterTextBox.TabIndex = 7;
			this.diameterTextBox.TextChanged += new System.EventHandler(this.EditTextValueChanged);
			this.diameterTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HandleKeyPress);
			// 
			// sizesGroupBox
			// 
			this.sizesGroupBox.BackColor = System.Drawing.SystemColors.Control;
			this.sizesGroupBox.Controls.Add(this.thicknessConstraintLabel);
			this.sizesGroupBox.Controls.Add(this.thicknessLabel);
			this.sizesGroupBox.Controls.Add(this.thicknessTextBox);
			this.sizesGroupBox.Controls.Add(this.widthConstraintLabel);
			this.sizesGroupBox.Controls.Add(this.heightConstraintLabel);
			this.sizesGroupBox.Controls.Add(this.lengthConstraintLabel);
			this.sizesGroupBox.Controls.Add(this.widthLabel);
			this.sizesGroupBox.Controls.Add(this.heightLabel);
			this.sizesGroupBox.Controls.Add(this.lengthLabel);
			this.sizesGroupBox.Controls.Add(this.widthTextBox);
			this.sizesGroupBox.Controls.Add(this.heightTextBox);
			this.sizesGroupBox.Controls.Add(this.lengthTextBox);
			this.sizesGroupBox.Location = new System.Drawing.Point(17, 6);
			this.sizesGroupBox.Name = "sizesGroupBox";
			this.sizesGroupBox.Size = new System.Drawing.Size(216, 138);
			this.sizesGroupBox.TabIndex = 3;
			this.sizesGroupBox.TabStop = false;
			this.sizesGroupBox.Text = "Габариты";
			// 
			// thicknessConstraintLabel
			// 
			this.thicknessConstraintLabel.Location = new System.Drawing.Point(144, 104);
			this.thicknessConstraintLabel.Name = "thicknessConstraintLabel";
			this.thicknessConstraintLabel.Size = new System.Drawing.Size(64, 20);
			this.thicknessConstraintLabel.TabIndex = 11;
			this.thicknessConstraintLabel.Text = "(1..2)";
			this.thicknessConstraintLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.thicknessConstraintLabel.Visible = false;
			// 
			// thicknessLabel
			// 
			this.thicknessLabel.Location = new System.Drawing.Point(8, 104);
			this.thicknessLabel.Name = "thicknessLabel";
			this.thicknessLabel.Size = new System.Drawing.Size(80, 20);
			this.thicknessLabel.TabIndex = 10;
			this.thicknessLabel.Text = "Толщина, мм";
			this.thicknessLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// thicknessTextBox
			// 
			this.thicknessTextBox.BackColor = System.Drawing.SystemColors.Window;
			this.thicknessTextBox.Enabled = false;
			this.thicknessTextBox.Location = new System.Drawing.Point(88, 104);
			this.thicknessTextBox.MaxLength = 2;
			this.thicknessTextBox.Name = "thicknessTextBox";
			this.thicknessTextBox.Size = new System.Drawing.Size(56, 20);
			this.thicknessTextBox.TabIndex = 9;
			this.thicknessTextBox.TextChanged += new System.EventHandler(this.EditTextValueChanged);
			this.thicknessTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HandleKeyPress);
			// 
			// widthConstraintLabel
			// 
			this.widthConstraintLabel.Location = new System.Drawing.Point(144, 64);
			this.widthConstraintLabel.Name = "widthConstraintLabel";
			this.widthConstraintLabel.Size = new System.Drawing.Size(64, 20);
			this.widthConstraintLabel.TabIndex = 8;
			this.widthConstraintLabel.Text = "(10..2000)";
			this.widthConstraintLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// heightConstraintLabel
			// 
			this.heightConstraintLabel.BackColor = System.Drawing.SystemColors.Control;
			this.heightConstraintLabel.Location = new System.Drawing.Point(144, 40);
			this.heightConstraintLabel.Name = "heightConstraintLabel";
			this.heightConstraintLabel.Size = new System.Drawing.Size(64, 20);
			this.heightConstraintLabel.TabIndex = 7;
			this.heightConstraintLabel.Text = "(10..200)";
			this.heightConstraintLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lengthConstraintLabel
			// 
			this.lengthConstraintLabel.BackColor = System.Drawing.SystemColors.Control;
			this.lengthConstraintLabel.Location = new System.Drawing.Point(144, 16);
			this.lengthConstraintLabel.Name = "lengthConstraintLabel";
			this.lengthConstraintLabel.Size = new System.Drawing.Size(64, 20);
			this.lengthConstraintLabel.TabIndex = 6;
			this.lengthConstraintLabel.Text = "(10..200)";
			this.lengthConstraintLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// widthLabel
			// 
			this.widthLabel.Location = new System.Drawing.Point(8, 64);
			this.widthLabel.Name = "widthLabel";
			this.widthLabel.Size = new System.Drawing.Size(80, 20);
			this.widthLabel.TabIndex = 5;
			this.widthLabel.Text = "Ширина, мм";
			this.widthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// heightLabel
			// 
			this.heightLabel.BackColor = System.Drawing.SystemColors.Control;
			this.heightLabel.Location = new System.Drawing.Point(8, 40);
			this.heightLabel.Name = "heightLabel";
			this.heightLabel.Size = new System.Drawing.Size(80, 20);
			this.heightLabel.TabIndex = 4;
			this.heightLabel.Text = "Высота, мм";
			this.heightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lengthLabel
			// 
			this.lengthLabel.BackColor = System.Drawing.SystemColors.Control;
			this.lengthLabel.Location = new System.Drawing.Point(8, 16);
			this.lengthLabel.Name = "lengthLabel";
			this.lengthLabel.Size = new System.Drawing.Size(80, 20);
			this.lengthLabel.TabIndex = 3;
			this.lengthLabel.Text = "Длина, мм";
			this.lengthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// widthTextBox
			// 
			this.widthTextBox.BackColor = System.Drawing.SystemColors.Window;
			this.widthTextBox.Location = new System.Drawing.Point(88, 64);
			this.widthTextBox.MaxLength = 4;
			this.widthTextBox.Name = "widthTextBox";
			this.widthTextBox.Size = new System.Drawing.Size(56, 20);
			this.widthTextBox.TabIndex = 2;
			this.widthTextBox.TextChanged += new System.EventHandler(this.EditTextValueChanged);
			this.widthTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HandleKeyPress);
			// 
			// heightTextBox
			// 
			this.heightTextBox.BackColor = System.Drawing.SystemColors.Window;
			this.heightTextBox.Location = new System.Drawing.Point(88, 40);
			this.heightTextBox.MaxLength = 3;
			this.heightTextBox.Name = "heightTextBox";
			this.heightTextBox.Size = new System.Drawing.Size(56, 20);
			this.heightTextBox.TabIndex = 1;
			this.heightTextBox.TextChanged += new System.EventHandler(this.EditTextValueChanged);
			this.heightTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HandleKeyPress);
			// 
			// lengthTextBox
			// 
			this.lengthTextBox.BackColor = System.Drawing.SystemColors.Window;
			this.lengthTextBox.Location = new System.Drawing.Point(88, 16);
			this.lengthTextBox.MaxLength = 3;
			this.lengthTextBox.Name = "lengthTextBox";
			this.lengthTextBox.Size = new System.Drawing.Size(56, 20);
			this.lengthTextBox.TabIndex = 0;
			this.lengthTextBox.TextChanged += new System.EventHandler(this.EditTextValueChanged);
			this.lengthTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HandleKeyPress);
			// 
			// toolTipHelp
			// 
			this.toolTipHelp.AutomaticDelay = 0;
			this.toolTipHelp.AutoPopDelay = 10000000;
			this.toolTipHelp.InitialDelay = 0;
			this.toolTipHelp.ReshowDelay = 0;
			this.toolTipHelp.UseAnimation = false;
			this.toolTipHelp.UseFading = false;
			// 
			// roundCheckBox
			// 
			this.roundCheckBox.Location = new System.Drawing.Point(24, 280);
			this.roundCheckBox.Name = "roundCheckBox";
			this.roundCheckBox.Size = new System.Drawing.Size(200, 24);
			this.roundCheckBox.TabIndex = 5;
			this.roundCheckBox.Text = "Закруглить угол";
			this.roundCheckBox.UseVisualStyleBackColor = true;
			this.roundCheckBox.CheckedChanged += new System.EventHandler(this.RoundCheckBoxCheckedChanged);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(250, 356);
			this.Controls.Add(this.roundCheckBox);
			this.Controls.Add(this.createButton);
			this.Controls.Add(this.holesGroupBox);
			this.Controls.Add(this.sizesGroupBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MainForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "Анкерный уголок";
			this.holesGroupBox.ResumeLayout(false);
			this.holesGroupBox.PerformLayout();
			this.sizesGroupBox.ResumeLayout(false);
			this.sizesGroupBox.PerformLayout();
			this.ResumeLayout(false);

		}
	}
}
