﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;

namespace plugin
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		
		/// <summary>
		/// Angle instance to hold and pass user-defined parameters to wrapper 
		/// </summary>
		private Angle _angle;
		
		/// <summary>
		/// A KompasWrapper instance to actually do stuff.
		/// </summary>
		private KompasWrapper _wrapper;
		
		/// <summary>
		/// MainForm constructor.
		/// </summary>
		public MainForm()
		{
			InitializeComponent();
			
			_angle = new Angle();
			_wrapper = new KompasWrapper();
		}
		
		/// <summary>
		/// Gets Angle's property name as string used in conjunction 
		/// with given control.
		/// </summary>
		/// <param name="control">TextBox to get property to.</param>
		/// <returns>ParameterEnum corresponding to TextBox</returns>
		private ParameterEnum GetEnumByControl(TextBox control)
		{	
			switch (control.Name)
			{
				case "lengthTextBox":
					return ParameterEnum.Length;
				case "widthTextBox":
					return ParameterEnum.Width;
				case "heightTextBox":
					return ParameterEnum.Height;
				case "diameterTextBox":
					return ParameterEnum.Diameter;
				case "perRowTextBox":
					return ParameterEnum.PerRow;
				case "bottomRowsTextBox":
					return ParameterEnum.RowsBottom;
				case "topRowsTextBox":
					return ParameterEnum.RowsTop;
				case "thicknessTextBox":
					return ParameterEnum.Thickness;
				default:
					throw new ArgumentException("Unknown control.");
			}
		}
		
		/// <summary>
		/// Checks whether current value in a given textbox is valid.
		/// </summary>
		/// <param name="control">Textbox to check.</param>
		/// <returns>True when valid, false otherwise.</returns>
		private bool Check(TextBox control)
		{
			bool ret;
			// digging my own grave here
			// desire to write less code is sometimes dangerous
			var reference = GetEnumByControl(control);
			Type t = typeof(Angle);
			// shady stuff here
			PropertyInfo p = t.GetProperty(reference.ToString());
			var minMax = _angle.GetMinMax(reference);
			string help = "";
			try
			{
					p.SetValue(_angle, int.Parse(control.Text));
					control.BackColor = SystemColors.Window;
					toolTipHelp.Hide(control);
					ret = true;
			}
			catch (TargetInvocationException ex)
			{
				if (ex.InnerException.GetType() == typeof(ConstrainedValueException))
				{
					//                     :(
					string comparison = ((ConstrainedValueException)(ex.InnerException)).Type 
						== ValueExceptionType.ValueTooBig ? "велико" : "мало";
					help = String.Format(
						"Введенное значение слишком {0}, введите число от {1} до {2}.",
						comparison, minMax.X, minMax.Y);
				}
				// else?
				control.BackColor = Color.Orange;
				toolTipHelp.Show(help, control);
				ret = false;
			}
			catch (FormatException)
			{
				help = String.Format("Введите число от {0} до {1}.", minMax.X, minMax.Y);
				control.BackColor = Color.Orange;
				toolTipHelp.Show(help, control);
				ret = false;
				
			}
			return ret;
		}
		
		/// <summary>
		/// Event handler for textbox changes. Enables and disables controls
		/// based on results of checking.
		/// </summary>
		/// <param name="sender">Event sender, any textbox.</param>
		/// <param name="e">Event arguments.</param>
		private void EditTextValueChanged(object sender, EventArgs e)
		{
			if (Check(lengthTextBox) && Check(heightTextBox) && Check(widthTextBox))
			{
				// if dimensions are valid, diameter is unlocked
				SetDiameterAndThicknessLock(true);
				// all calls to tooltip.Hide 
				// suppress previous tooltips with an outdated text
				toolTipHelp.Hide(diameterTextBox);
				if (Check(thicknessTextBox) && Check(diameterTextBox))
				{
					// if diameter is valid, holes controls are unlocked
					SetHoleNumbersLock(true);
					toolTipHelp.Hide(perRowTextBox);
					toolTipHelp.Hide(bottomRowsTextBox);
					toolTipHelp.Hide(topRowsTextBox);
					if (Check(perRowTextBox) && Check(bottomRowsTextBox)
					    && Check(topRowsTextBox))
					{
						// if hole numbers are valid, create button
						// is finally useable
						createButton.Enabled = true;
					}
					// everything following a failed check is locked
					else
					{
						createButton.Enabled = false;
					}
				}
				else
				{
					SetHoleNumbersLock(false);
				}
			}
			else
			{
				SetDiameterAndThicknessLock(false);
			}
			
		}
		
		/// <summary>
		/// Locks and unlocks diameter and thickness controls,
		/// also updates their help labels, which is a violation of SRP.
		/// </summary>
		/// <param name="state">Lock state: true is unlocked, false is locked.</param>
		// should get an enum for this ^
		private void SetDiameterAndThicknessLock(bool state)
		{
			diameterTextBox.Enabled = state;
			diameterConstraintLabel.Visible = state;
			diameterConstraintLabel.Text = String.Format(
				"(1..{0})", _angle.GetMinMax(ParameterEnum.Diameter).Y);
			
			thicknessTextBox.Enabled = state;
			thicknessConstraintLabel.Visible = state;
			thicknessConstraintLabel.Text = String.Format(
				"(1..{0})", _angle.GetMinMax(ParameterEnum.Thickness).Y);
			
			// if we're locking, we also lock all depending controls
			if (!state)
			{
				SetHoleNumbersLock(state);
			}
		}
		
		/// <summary>
		/// Locks and unlocks hole number controls, also updates their help labels,
		/// which is a violation of SRP.
		/// </summary>
		/// <param name="state">Lock state: true is unlocked, false is locked.</param>
		private void SetHoleNumbersLock(bool state)
		{
			perRowTextBox.Enabled = state;
			perRowConstraintLabel.Visible = state;
			perRowConstraintLabel.Text = String.Format(
				"(1..{0})", _angle.GetMinMax(ParameterEnum.PerRow).Y);
			
			topRowsTextBox.Enabled = state;
			topRowsConstraintLabel.Visible = state;
			topRowsConstraintLabel.Text = String.Format(
				"(1..{0})",_angle.GetMinMax(ParameterEnum.RowsTop).Y);
			
			bottomRowsTextBox.Enabled = state;
			bottomRowsConstraintLabel.Visible = state;
			bottomRowsConstraintLabel.Text = String.Format(
				"(1..{0})", _angle.GetMinMax(ParameterEnum.RowsBottom).Y);
			// if these are locked, the button should be locked too
			if (!state)
			{
				createButton.Enabled = false;
			}
		}
		
		/// <summary>
		/// Textbox KeyPress handler. Dismisses anything 
		/// that is not number or control symbol.
		/// User still can paste anything, but it's better than nothing.
		/// </summary>
		/// <param name="sender">Event sender, any textbox from the form.</param>
		/// <param name="e">Event arguments.</param>
		private void HandleKeyPress(object sender, KeyPressEventArgs e)
		{
			// see https://stackoverflow.com/questions/463299/
			e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
		}
		
		/// <summary>
		/// Handler for createButton click. Calls KompasWrapper.CreateAngle with
		/// form's _angle as an argument.
		/// </summary>
		/// <param name="sender">Event sender, createButton.</param>
		/// <param name="e">Event arguments.</param>
		private void CreateButtonClick(object sender, EventArgs e)
		{
			try
			{
				_wrapper.CreateAngle(_angle);
			}
			catch (Exception ex)
			{
				MessageBox.Show(String.Format(
					"Ой, что-то пошло не так! Можно попробовать ещё раз.\nТехнические подробности: {0}", ex), 
					"Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		
		/// <summary>
		/// Handler for checkbox click. Sets _angle's Rounded property.
		/// </summary>
		/// <param name="sender">Event sender, roundCheckBox.</param>
		/// <param name="e">Event arguments.</param>
		private void RoundCheckBoxCheckedChanged(object sender, EventArgs e)
		{
			_angle.Rounded = roundCheckBox.Checked;
		}
	}
}
