﻿namespace plugin
{
	/// <summary>
	/// Enumeration that holds possible Angle parameters.
	/// </summary>
	public enum ParameterEnum 
	{
		Length,
		Width,
		Height,
		Diameter,
		Thickness,
		PerRow,
		RowsTop,
		RowsBottom
	};
}