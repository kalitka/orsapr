﻿namespace plugin
{
	/// <summary>
	/// Enumeration that holds possible value mismatch types
	/// </summary>
	public enum ValueExceptionType 
	{
		ValueTooBig,
		ValueTooSmall
	};
}