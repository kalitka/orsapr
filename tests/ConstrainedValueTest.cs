﻿using System;
using NUnit.Framework;
using plugin;

namespace tests
{
	/// <summary>
	/// Tests for ConstrainedValue class.
	/// All non-constructor tests are conducted on a ConstrainedValue(10, 20, 30)
	/// </summary>
	[TestFixture]
	public class ConstrainedValueTest
	{
		/// <summary>
		/// Initial Min value in non-constructor tests.
		/// </summary>
		private const int _initialMin = 10;
		
		/// <summary>
		/// Initial Value in non-constructor tests.
		/// </summary>
		private const int _initialValue = 20;
		
		/// <summary>
		/// Initial Max value in non-constructor tests.
		/// </summary>
		private const int _initialMax = 30;
		
		/// <summary>
		/// Positive test for ConstrainedValue's constructor.
		/// </summary>
		/// <param name="min">Minimum parameter to pass to constructor.</param>
		/// <param name="val">Value parameter to pass to constructor.</param>
		/// <param name="max">Maximum parameter to pass to constructor.</param>
		[Test(Description = "Constructor positive test.")]
		[TestCase(1, 2, 3, TestName = "Basic test.")]
		[TestCase(1, 1, 3, TestName = "Value is equal to min test.")]
		[TestCase(1, 3, 3, TestName = "Value is equal to max test.")]
		[TestCase(4, 4, 4, TestName = "All parameters are equal.")]
		public void ConstrainedValueConstructorPositiveTest(int min, int val, int max)
		{
			var CVToTest = new ConstrainedValue(min, val, max);
			Assert.Multiple(() => {
				Assert.That(CVToTest.Min, Is.EqualTo(min));
				Assert.That(CVToTest.Value, Is.EqualTo(val));
				Assert.That(CVToTest.Max, Is.EqualTo(max));
			});
		}
		
		/// <summary>
		/// Negative test for ConstrainedValues's constructor.
		/// Should *probably* be divided to two tests for each exception?
		/// </summary>
		/// <param name="min">Minimum parameter to pass to constructor.</param>
		/// <param name="val">Value parameter to pass to constructor.</param>
		/// <param name="max">Maximum parameter to pass to constructor.</param>
		/// <param name="expected">Expected type of ConstrainedValueException.Type
		/// null if we're catching different exception.</param>
		[Test(Description = "Constructor negative test.")]
		[TestCase(1, 4, 3, ValueExceptionType.ValueTooBig, TestName = "Value too big test.")]
		[TestCase(2, 1, 3, ValueExceptionType.ValueTooSmall, TestName = "Value too small test.")]
		[TestCase(5, 3, 2, null, TestName = "Min > Max test.")]
		[TestCase(-1, 2, 3, null, TestName = "Min out of bounds test.")]
		[TestCase(1, 2, 3000, null, TestName = "Max out of bounds test.")]
		[TestCase(2001, 2002, 2003, null, TestName = "Everything out of bounds test.")]
		public void ConstrainedValueConstructorNegativeTest(int min, int val, int max, ValueExceptionType expected)
		{
			// how to code, halp
			try
			{
				var CV = new ConstrainedValue(min, val, max);
				throw new AssertionException("The code above should always throw an exception.");
			}
			catch (ArgumentException)
			{
				// we got our ArgumentException, all is ok
				Assert.That(true);
			}
			catch (ConstrainedValueException ex)
			{
				Assert.That(ex.Type, Is.EqualTo(expected));
			}
			catch
			{
				throw new AssertionException("Unknown exception catched.");
			}
		}
		
		/// <summary>
		/// Min property positive test.
		/// </summary>
		/// <param name="newMin">New Min value to set.</param>
		[Test(Description = "Min property positive test.")]
		[TestCase(5, TestName = "Min lowering test.")]
		[TestCase(15, TestName = "Min raising without value changes test.")]
		[TestCase(20, TestName = "Min raising to current value test.")]
		[TestCase(25, TestName = "Min raising that also changes value test.")]
		public void ConstrainedValueMinPositiveTest(int newMin)
		{
			var CV = new ConstrainedValue(_initialMin, _initialValue, _initialMax);
			CV.Min = newMin;
			Assert.That(CV.Min, Is.EqualTo(newMin));
			if (newMin >= _initialValue)
				Assert.That(CV.Value, Is.EqualTo(newMin));
		}
		
		/// <summary>
		/// Min property negative test.
		/// </summary>
		/// <param name="newMin">New Min value to set.</param>
		[Test(Description = "Min property negative test.")]
		[TestCase(40, TestName = "Min > Max test.")]
		[TestCase(-100, TestName = "Min outside of bounds test.")]
		[TestCase(2018, TestName = "Min > Max and also out of bounds test.")]
		public void ConstrainedValueMinNegativeTest(int newMin)
		{
			var CV = new ConstrainedValue(_initialMin, _initialValue, _initialMax);
			TestDelegate exceptionGenerator = () => CV.Min = newMin;
			Assert.Throws(typeof(ArgumentException), exceptionGenerator);
		}
		
		/// <summary>
		/// Max property positive test.
		/// </summary>
		/// <param name="newMax">New Max value to set.</param>
		[Test(Description = "Max property positive test.")]
		[TestCase(50, TestName = "Max raising test.")]
		[TestCase(25, TestName = "Max lowering without value changes test.")]
		[TestCase(20, TestName = "Max lowering to current value test.")]
		[TestCase(15, TestName = "Max lowering below current value test.")]
		public void ConstrainedValueMaxPositiveTest(int newMax)
		{
			var CV = new ConstrainedValue(_initialMin, _initialValue, _initialMax);
			CV.Max = newMax;
			Assert.That(CV.Max, Is.EqualTo(newMax));
			if (newMax <= _initialValue)
				Assert.That(CV.Value, Is.EqualTo(newMax));
		}
		
		/// <summary>
		/// Max property negative test.
		/// </summary>
		/// <param name="newMax">New Max value to set.</param>
		[Test(Description = "Max property negative test.")]
		[TestCase(5, TestName = "Max < Min test.")]
		[TestCase(2007, TestName = "Max outside of bounds test.")]
		[TestCase(-1000, TestName = "Max < Min and out of bounds test.")]
		public void ConstrainedValueMaxNegativeTest(int newMax)
		{
			var CV = new ConstrainedValue(_initialMin, _initialValue, _initialMax);
			TestDelegate exceptionGenerator = () => CV.Max = newMax;
			Assert.Throws(typeof(ArgumentException), exceptionGenerator);
		}
		
		/// <summary>
		/// Value property positive test.
		/// </summary>
		/// <param name="newValue">New Value to set.</param>
		[Test(Description = "Value property positive test.")]
		[TestCase(25, TestName = "Value change within bounds test.")]
		[TestCase(_initialMin, TestName = "Value = Min test.")]
		[TestCase(_initialMax, TestName = "Value = Max test.")]
		public void ConstrainedValueValuePositiveTest(int newValue)
		{
			var CV = new ConstrainedValue(_initialMin, _initialValue, _initialMax);
			CV.Value = newValue;
			Assert.That(CV.Value, Is.EqualTo(newValue));
		}
		
		/// <summary>
		/// Value property negative test.
		/// </summary>
		/// <param name="newValue">New Value to set.</param>
		/// <param name="expected">Expected type of ValueExceptionType 
		/// in resulting ConstrainedValueException.</param>
		[Test(Description = "Value property negative test.")]
		[TestCase(_initialMin - 1, ValueExceptionType.ValueTooSmall, TestName = "Lower than Min Value test.")]
		[TestCase(_initialMax + 2, ValueExceptionType.ValueTooBig, TestName = "Higher than Max Value test.")]
		public void ConstrainedValueValueNegativeTest(int newValue, ValueExceptionType expected)
		{
			var CV = new ConstrainedValue(_initialMin, _initialValue, _initialMax);
			try
			{
				CV.Value = newValue;
				throw new AssertionException("The line above should always throw a ConstrainedValueException");
			}
			catch (ConstrainedValueException ex)
			{
				Assert.That(ex.Type, Is.EqualTo(expected));
			}
			catch
			{
				throw new AssertionException("Unknown exception catched.");
			}
		}
	}
}
